'''
$Id: phase_shift_figure.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 21 Jul 2014 11:52:31 CEST

make a figure to illustrate phase shifting

$modified: Tue 17 Mar 2015 13:25:45 CET
  options for colours
'''

import matplotlib.pyplot as plt
import numpy as np
from math import *

def phase_shift_figure(pmin=None,pmax=None,tmin=None,tmax=None,shift=None,colours=None):

    if colours==None:
        colours={}
        colours['background']='black'
        colours['signal']='white'
        colours['wavefront']='red'
        
    
    if shift==None:
        shift_rad=0.5*pi
        shift=degrees(shift_rad)
    else:
        shift_rad=radians(shift)
    subttl='shifted by '+str('%.1f degrees' % shift)


    one_wvln=np.empty(101)
    t=np.empty(101)

    for n in range(101):
        arg=n*2*pi/100.
        t[n]=float(n)/100.
        one_wvln[n]=sin(arg)
        

    ttl='Phase Shifting (figure 1)'
    fig=plt.figure(figsize=(12.80,9.60))
    fig.canvas.set_window_title('plt: '+ttl) 

    ax = fig.add_subplot(111)
    ax.set_axis_bgcolor(colours['background'])

    axes=[0,7,-1.1,4.1]
    if isinstance(tmin,float) or isinstance(tmin,int):
        axes[0]=tmin
    if isinstance(tmax,float) or isinstance(tmax,int):
        axes[1]=tmax
    if isinstance(pmin,float) or isinstance(pmin,int):
        axes[2]=pmin
    if isinstance(pmax,float) or isinstance(pmax,int):
        axes[3]=pmax
    ax.axis(axes)

    pmin=axes[2]
    pmax=axes[3]


    # signal 1
    tt=t
    plt.plot(tt,one_wvln,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln,lw=4,color=colours['wavefront'])
    tt=tt+1
    plt.plot(tt,one_wvln,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln,lw=4,color=colours['signal'])

    # signal 2
    tt=t+1.75
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['wavefront'])
    tt=tt+1
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['signal'])

    # vertical line where signal will be probed
    plt.plot([4,4],[pmin,pmax],'g')

    plt.savefig('phase_shift_figure1.png',format='png')
    plt.show()

    ##########################################################
    # second plot: show phase shifted signal
    ttl='Phase Shifting (figure 2)'
    fig=plt.figure(figsize=(12.80,9.60))
    fig.canvas.set_window_title('plt: '+ttl) 
    ax = fig.add_subplot(111)
    ax.axis(axes)
    ax.set_axis_bgcolor(colours['background'])

    plt.title(subttl)

    # signal 1
    tt=t
    plt.plot(tt,one_wvln,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln,lw=4,color=colours['wavefront'])

    # adjust phase at 4 wavelengths.  shift by 135 degrees
    phase_shifted=np.empty(101)
    tt=tt+1
    for n in range(101):
        if tt[n]<3.5:
            phase_shifted[n]=one_wvln[n]
        else:
            arg=n*2*pi/100.
            phase_shifted[n]=sin(arg+shift_rad)
    plt.plot(tt,phase_shifted,lw=4,color=colours['signal'])

    # now shift the whole wvln
    for n in range(101):
        arg=n*2*pi/100.
        phase_shifted[n]=sin(arg+shift_rad)
    
    tt=tt+1
    plt.plot(tt,phase_shifted,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,phase_shifted,lw=4,color=colours['signal'])



    # signal 2
    tt=t+1.75
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['wavefront'])
    tt=tt+1
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['signal'])
    tt=tt+1
    plt.plot(tt,one_wvln+2.5,lw=4,color=colours['signal'])

    # vertical line where signal will be probed
    plt.plot([4,4],[pmin,pmax],'g')

    plt.savefig('phase_shift_figure2.png',format='png')
    plt.show()

    return
