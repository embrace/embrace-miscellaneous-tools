"""
$Id: datefunctions.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 26 Aug 2013 12:26:05 CEST

some useful date functions 
take from tools_embraceStats and from ObsFunctions

$modified: Thu 05 Sep 2013 12:12:21 CEST
  adding more possibilities to str2dt : assuming a time on the given day

$modified: Mon 07 Jul 2014 17:05:32 CEST
  str2dt() : correctly deal with FITS comment on multiple lines

$modified: Wed 25 Feb 2015 11:48:52 CET
  str2dt() : added format for isodate used in filenames for pulsar data

$modified: Tue  7 Apr 15:37:43 CEST 2015
  roundTime() : round time to nearest minute (or whatever) 

$modified: Wed 10 Jun 2015 13:57:17 CEST
  str2dt() : read posixly correct date format

$modified: Thu 16 Jul 2015 15:07:19 CEST
  str2dt() : read date in format: Jul 16 15:10:52  2015 for LCU pinglog

$modified: Fri 14 Aug 2015 15:28:50 CEST
  str2dt() : read date in stupid day.month.year format

$modified: Fri 29 Jul 2016 15:25:25 CEST
  str2dt() : now = NOW

"""
import datetime as dt
import re

def isodate(date):
    return date.strftime('%Y-%m-%d %H:%M:%S.%f UT')

def str2dt(datestr):
    if isinstance(datestr,dt.datetime): return datestr

    if not isinstance(datestr,str):
        print 'date conversion:  argument must be a string'
        return None

    dtnow=dt.datetime.utcnow()
    if datestr=="tomorrow":
        return dt.datetime(dtnow.year,dtnow.month,dtnow.day)+dt.timedelta(days=1)

    if datestr.upper()=="NOW": return dtnow

    datestr=datestr.replace("CONTINUE","").replace("'","") # fits file comment continued on next line
    datestr=re.sub(' UT.*','',datestr)
    fmts=["%Y-%m-%d %H:%M:%S.%f",
          "%Y-%m-%d %H:%M:%S",
          "%Y-%m-%d %H:%M:%SZ",
          "%Y-%m-%d %H:%M:%S.%fZ",
          '%Y-%m-%d %H:%M:%S.%f UT',
          '%Y-%m-%d %H:%M:%S UT',
          "%Y-%m-%d %H:%M",
          "%Y-%m-%d",
          "%Y%m%d-%H%M",
          "D%Y%m%dT%H%M%S",
          "%a %b %d %H:%M:%S %Y",
          "%b %d %H:%M:%S  %Y",
          "%d.%m.%Y  %H:%M"]

    for fmt in fmts:
        try: return dt.datetime.strptime(datestr,fmt)
        except: pass

    # more special cases, assuming some time today
    ymd=dtnow.strftime('%Y-%m-%d')
    datestr_today=ymd+' '+datestr
    for fmt in fmts:
        try: return dt.datetime.strptime(datestr_today,fmt)
        except: pass    


    print "did not convert date string to datetime.  returning None: "+datestr
    return None

# convert a timedelta to total number of seconds
def tot_seconds(delta):
    tsecs=delta.days*24*3600 + delta.seconds + delta.microseconds*1e-6
    return tsecs

# round time to nearest minute (or whatever)
# taken from stackoverflow
# http://stackoverflow.com/questions/3463930/how-to-round-the-minute-of-a-datetime-object-python/10854034#10854034
def roundTime(d=None, roundTo=60):
   """Round a datetime object to any time laps in seconds
   dt : datetime.datetime object, default now.
   roundTo : Closest number of seconds to round to, default 1 minute.
   Author: Thierry Husson 2012 - Use it as you want but don't blame me.
   """
   if d == None : d = dt.datetime.utcnow()
   seconds = (d - d.min).seconds
   # // is a floor division, not a comment on following line:
   rounding = (seconds+roundTo/2) // roundTo * roundTo
   return d + dt.timedelta(0,rounding-seconds,-d.microsecond)
