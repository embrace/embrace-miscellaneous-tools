'''
$Id: tileset_positions.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed May 23 17:54:57 CEST 2012

generate the AntennaArrays.conf file for comparison with LCU

$modified: Mon 30 Sep 2013 15:54:20 CEST
  compare how the individual elements appear in the LCU version
  i.e. the 4608 elements

$modified: Mon 14 Oct 2013 11:06:46 CEST
  adding a weird factor of 2 in the tileset positions
  which was discovered empirically and appears to be necessary when
  using reference by tileset in the beamforming
  to be continued...


'''
import os
AntennaArrays_origfile='AntennaArrays-I0_ref-by-Tileset.conf'
if not os.path.exists(AntennaArrays_origfile):
    print 'cannot find file for comparison: ',AntennaArrays_origfile
    quit()

import numpy as np
from math import *
zero=1e-4 # pretty crap rounding tolerance between C++ and Python

### rotation of the array w.r.t. N-S
# measured mechanically and astronomically
# measured by Laurence Alsac 2012-06-22
# confirmed by Henrik ...
rotang_deg=131.2857
#rotang_deg=rotang_deg-180

#rotang_deg=-39
#rotang_deg=45
#rotang_deg=0
rotang=radians(rotang_deg)

len_units=" m"
vivaldi_width=0.125

tile_width=vivaldi_width*6/sqrt(2)
tileset_width=4*tile_width
array_width=4*tileset_width

vivaldi_maxno_NS=23+1+23+1+23+1+23 # see Chenanceau notebook 2012-05-22
vivaldi_maxseparation_NS=vivaldi_maxno_NS-1

vivaldi_maxno_EW=4*24
vivaldi_maxseparation_EW=vivaldi_maxno_EW-1


print "Vivaldi spacing: ",vivaldi_width,len_units
print "tile width: ",tile_width,len_units
print "tileset width: ",tileset_width,len_units
print "array width: ",array_width,len_units

print "maximum number of Vivaldi on a line E-W: ",vivaldi_maxno_EW
print "separation between most distant Vivaldis E-W: ",vivaldi_maxseparation_EW*vivaldi_width,len_units
print "maximum number of Vivaldi on a line N-S: ",vivaldi_maxno_NS
print "separation between most distant Vivaldis N-S: ",vivaldi_maxseparation_NS*vivaldi_width,len_units

print "\n"

# position matrix for 4x4 tileset array
# 0,0 is lower left, and rotating by rotang
#tileset_width=1.
prime_axis=np.zeros( (16,3) )
print "position matrix for a rotation angle of ",rotang_deg," degrees"
n=0
for j in range(0,4):
    y=j*tileset_width  # weird factor of 0 not
    for i in range(0,4):
        x=i*tileset_width  # weird factor of 0 not
        x_prime=x*cos(rotang)-y*sin(rotang) 
        y_prime=x*sin(rotang)+y*cos(rotang) 
        print "%10.6f " % x_prime,
        print "%10.6f" % y_prime
        prime_axis[n][0]=x_prime
        prime_axis[n][1]=y_prime
        n+=1




print '\ncompare with '+AntennaArrays_origfile
ifile=open(AntennaArrays_origfile,'r')
line=ifile.readline()
while line!='':
    if line.strip()=='16 x 3':
        print 'found start at ',ifile.tell()
        break
    line=ifile.readline()

antarray=np.zeros( (16,3) )
n=0
while n<16:
    line=ifile.readline().replace('[','').replace(']','')
    row=line.split()
    antarray[n,:]=row
    n+=1
ifile.close()


n=0
while n<16:
    print "%10.6f " % antarray[n][0],
    print "%10.6f" % antarray[n][1]
    n+=1

print '\nline by line'
n=0
while n<16:
    print 'antarray: %10.6f %10.6f' % (antarray[n][0],antarray[n][1])
    print 'prime ax: %10.6f %10.6f' % (prime_axis[n][0],prime_axis[n][1]),'\n'
    n+=1



print "\nthe difference (only non-zero shown)"
n=0
while n<16:
    x_diff=prime_axis[n][0]-antarray[n][0]
    y_diff=prime_axis[n][1]-antarray[n][1]
    if abs(x_diff)>zero or abs(y_diff)>zero:
        print "line %4i : %10.6f %10.6f" % (n,x_diff,y_diff)
    n+=1


# make an AntennaArrays.conf file
ofile=open('new.AntennaArrays.conf','w')
headertxt='TILESET-NANCAY\n3\n[    2.1992   47.3819       100  ]\n16 x 3'
ofile.write(headertxt)
firstline=True
startchar='\n['
for n in range(0,16):
    ofile.write(startchar+str("%10.6f %10.6f 0" % (prime_axis[n][0],prime_axis[n][1])))
    if firstline:
        firstline=False
        startchar='\n'
ofile.write(' ]')


#rotang=0
#vivaldi_width=1#/sqrt(2.)

## right across the array, or by tile?
headertxt='\n\n4608 x 3'
ofile.write(headertxt)
firstline=True
startchar='\n['
element_array=np.zeros( (4608,3) )
n=0

###################################################################################
# try by hexboard
# every other row, the x begins halfway further (i.e. add 1/sqrt(2))
# note that vivaldi width is nominally lambda/2
# see paper by JP, and my chenanceau notebook
for ntileset_y in range(0,4):
    for ntileset_x in range(0,4):
        for ntile_y in range(0,2):
            for ntile_x in range(0,2):
                for nhex_y in range(0,2):
                    for nhex_x in range(0,3):
                        for j in range(0,6):

                            # the following is referring the whole array to one point
                            #y=vivaldi_width*sqrt(2.)*( nhex_y*6/2. + j/2. + ntile_y*6 + ntileset_y*12 )

                            # the following is making each tileset refer to its own reference point
                            y=vivaldi_width*sqrt(2.)*( nhex_y*6/2. + j/2. + ntile_y*6  )

                            # the following is making each tile refer to its own reference point
                            #y=vivaldi_width*sqrt(2.)*( nhex_y*6/2. + j/2.   )

                    
                            if j in [1,3,5]: xoffset=0.5
                            else: xoffset=0.0
                            for i in range(0,2):
                                # the following is referring the whole array to one point
                                #x=vivaldi_width*sqrt(2.)*( xoffset + nhex_x*2 + i + ntile_x*6 + ntileset_x*12 )

                                # the following is making each tileset refer to its own reference point
                                x=vivaldi_width*sqrt(2.)*( xoffset + nhex_x*2 + i + ntile_x*6  )

                                # the following is making each tile refer to its own reference point
                                #x=vivaldi_width*sqrt(2.)*( xoffset + nhex_x*2 + i )

                                x_prime=x*cos(rotang)-y*sin(rotang)
                                y_prime=x*sin(rotang)+y*cos(rotang)
                                ofile.write(startchar+str("%10.6f " % x_prime)+str("%10.6f" % y_prime)+" 0")
                                element_array[n][0]=x_prime
                                element_array[n][1]=y_prime

                                if firstline:
                                    firstline=False
                                    startchar='\n '
                                n+=1
###################################################################################
ofile.write(' ]\n')
ofile.close()

print '\ncompare with '+AntennaArrays_origfile
ifile=open(AntennaArrays_origfile,'r')
line=ifile.readline()
while line!='':
    if line.strip()=='4608 x 3':
        print 'found start at ',ifile.tell()
        break
    line=ifile.readline()

origarray=np.zeros( (4608,3) )
n=0
while n<4608:
    line=ifile.readline().replace('[','').replace(']','')
    row=line.split()
    origarray[n,:]=row
    n+=1
ifile.close()

print "\nthe difference (only non-zero shown)"
n=0
while n<4608:
    x_diff=element_array[n][0]-origarray[n][0]
    y_diff=element_array[n][1]-origarray[n][1]
    if abs(x_diff)>zero or abs(y_diff)>zero:
        print "line %4i : %10.6f %10.6f" % (n,x_diff,y_diff)
    n+=1
