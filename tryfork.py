'''
example modified from:
http://www.python-course.eu/forking.php

and then from:
http://stackoverflow.com/questions/871447/problem-with-a-python-program-using-os-pipe-and-os-fork
'''

import os,sys,gc,psutil,time
import numpy as np
import pyfits as fits
def childlabour(n,opipe):

    print 'A new child ',  os.getpid()

    '''
    print "creating a big array"
    x=range(20000000)
    print "looping through array and doing some arithmetic"
    for i in x:
        x[i]=i*10+5

    print "picking out value: n=",n," x[n]=",x[n]
    retval=x[n]

    opipe.write(str("%i %i" % (os.getpid(),retval))+'\n')
    '''

    hdulist=fits.open('Sun_BeamA_bsx_20130514-0921.fits')
    opipe.write(str(hdulist))
    opipe.close()
    print "that's all.  exiting ..."
    #time.sleep(20)
    print "bye"
    os._exit(0)  
    return

def parentalanxiety(n=16):

    r,w=os.pipe()
    r,w=os.fdopen(r,'r',0), os.fdopen(w,'w',0)

    newpid = os.fork()
    if newpid == 0:
        childlabour(n,w)
            
    else:
        print "parent: %d, child: %d" % (os.getpid(),newpid)
        maxcount=100
        counter=0
        #while counter <= maxcount:
        hdulist=r.readline()
        #if not data: break
        #print "parent read: " + data.strip()
        counter+=1
        os.wait()


    r.close()
    return hdulist


hdulist=parentalanxiety()
#print 'parental anxiety returned. check the first few values of p'

