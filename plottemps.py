#!/usr/bin/python
"""
$Id: python script to plot data from the temperature sensor
$auth: Steve Torchinsky (torchinsky@obs-nancay.fr)
$created: Wed Jun  1 13:50:15 CEST 2011

plot temperature data from the sensor in the EMBRACE container
the data is read from file $HOME/data/embracetemperatures.txt
the data file has been filled with data using the script
  embracetemperature.py

temperature data is polled every 10 minutes with a cron job
and a series of temperatures are read each time
the data may be duplicated in that file so this script runs a unique sort

$modified: Thu 08 Jan 2015 17:07:53 CET
  use datetime for x-axis instead of converting to minutes or whatever

"""
# import stuff
import time
import datetime as dt
import os
import sys
import matplotlib.pyplot as plt

show=False
timescale=60.0
timeunits='minutes'
psym='+'
for arg in sys.argv:
    if arg=='--show':
        show=True
    if arg=='--hours':
        timescale=3600.0
        timeunits='hours'
    if arg=='--days':
        timescale=3600.0*24.0
        timeunits='days'
    if arg=='--lines':
        psym='-'

# open the data file
datafile=os.environ['HOME']+'/data/embracetemperatures.txt'
if not os.path.exists(datafile):
    print 'ERROR: file not found: '+datafile
    quit()
    
ifile=open(datafile,'r')

# read the data
data=[]
for line in ifile:
    datstr=line.rsplit(' ')
    data.append( [eval(datstr[0]) , eval(datstr[1])] )
ifile.close()

# remove duplicate entries by making it into a set
# sort the data in chronological order, and remove duplicates
dataset = sorted(set( [tuple(xx) for xx in data] ))

# refer to start time, and convert to timescale
t0=dataset[0][0]

# fill data arrays, but filter out zero temperature entries
# the sensor always puts two zeros at the start of each sequence of measures
t=[]
date=[]
deg=[]
for dat in dataset:
    if dat[1] != 0:
        t.append( (dat[0]-t0)/timescale )
        date.append(dt.datetime.fromtimestamp(dat[0]))
        deg.append( dat[1] )


# translate t0 to a calender date
t0_struc=time.localtime(t0)
t0_str=time.strftime('%d %b %Y %H:%M:%S',t0_struc)

# translate t_final to a calender date
tf=dataset[len(dataset)-1][0]
tf_struc=time.localtime(tf)
tf_str=time.strftime('%d %b %Y %H:%M:%S',tf_struc)


# plot function
#def plotit():

if not show:
    plt.ioff()

fig=plt.figure(figsize=(10.24,7.68))
fig.canvas.set_window_title('plt: EMBRACE Temperature') 
plt.xlabel('time since '+t0_str+' until '+tf_str)
plt.ylabel('Temperature / Celsius')
plt.title('EMBRACE Temperature in the Container')
ret=plt.plot(date,deg,psym)
ret=plt.savefig(os.environ['HOME']
                +'/work/data/embracetemperatures_'
                +time.strftime('%Y%m%d-%H%M',tf_struc)
                +'.png',dpi=100)
if not show:
    plt.close()
else:
    ret=plt.show()

# spawn a process to plot so we can continue interactively
#from multiprocessing import Process
#p = Process(target=plotit)
#p.start()

