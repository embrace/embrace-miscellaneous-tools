#!/usr/bin/python
import sys,os
import subprocess
import socket
import IN
import datetime
utcnow=datetime.datetime.utcnow()

integrateStep = 10
lane = 0  #0,1,2,3
port = 4346
hostIPs = ['10.11.0.255','10.11.1.77','10.11.2.77','10.11.3.77']
#host = ''
#seqNr=15
#logFileName="/mnt/lane0/udpdump.log"
logFileName = "/home/artemis/EMBRACE/dump/logs/udpdump.log"
dataDir = "/home/artemis/EMBRACE/dump/laneB"
filenameBase = "EMBRACEnancay_"+utcnow.strftime('%Y%m%dT%H%M%S')
ext = ".epackets"
REC_SET = True
bufsize=131072
#datapath=dataDir+filenameBase+ext


def checkForDataIn(lane):
  clientsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  clientsock.bind((hostIPs[lane], port+lane))

#Wait until I get some data:
  print "Waiting..."
  recv_msg, addr = clientsock.recvfrom(bufsize)
  print "...got some data.",

def getAdataBurst(lane,seqNr):
  try:
    clientsock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
  except socket.error as msg:
    print msg 
  try:
    #clientsock.setsockopt(socket.SOL_SOCKET, IN.SO_BINDTODEVICE,"eth1.201"+str(lane+1)+'\0')
    pass
  except socket.error as msg:
    print msg
  clientsock.bind((hostIPs[lane], port+lane))
  #print hostIPs[lane]

#Wait until I get some data:
  print "Waiting..."
  recv_msg, addr = clientsock.recvfrom(bufsize)
  startTime=datetime.datetime.utcnow()
  print "...got some data at ",startTime,
  if REC_SET==True:
    print "Recording until data-stream times-out."
    datapath=dataDir+str(lane)+"/"+filenameBase+"_seq"+str(seqNr)+"_lane"+str(lane)+ext
    f=open(datapath, "wb")
    f.write(recv_msg)
  clientsock.settimeout(1.0)
  while True :
     try:
       recv_msg, addr = clientsock.recvfrom(bufsize)
     except socket.timeout:
       clientsock.close()
       if REC_SET==True:
         f.close()
       break
     if REC_SET==True:
       f.write(recv_msg)
  return startTime,datapath

def startLane(lane):
  lfh=open(logFileName,"r");
  fLineLst=lfh.readlines();
  lfh.close();
  try: # try to read the last sequence number
    seqNr=int(fLineLst[-1].split(",",2)[0])
  except: # otherwise it's a new file, and start with 1
    seqNr=1
  while seqNr :
    seqNr=seqNr+1
    startTime,datapath=getAdataBurst(lane,seqNr)
    stopTime=datetime.datetime.utcnow()
    lfh=open(logFileName,"a");
    lfh.write(str(seqNr)+","+str(lane)+","+str(startTime)+","+str(stopTime-startTime)+'\n')
    lfh.close()

if __name__=="__main__":
   if len(sys.argv) > 1:
     lane=int(sys.argv[1])
   else:
     lane=0

   print 'dumping data from lane ',lane


   # create log file if it doesn't exist
   if not os.path.exists(logFileName):
     logfile=open(logFileName,'w')
     logfile.close()


   # start dumping
   startLane(lane)
