#!/usr/bin/python
"""
# task to convert a list of julian dates to gregorian dates
# description at http://mathforum.org/library/drmath/view/51907.html
# Original algorithm in Jean Meeus, "Astronomical Formulae for Calculators"

$Id: jd2gd.py
$auth: Enno Middelberg 2002

$modified: Thu 22 Aug 2013 14:27:39 CEST
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
adapted so it can be imported as a python function, and return a datetime object
"""
import math, sys, string
import datetime as dt

def jd2gd(x):
    if not isinstance(x,float) and not isinstance(x,int):
        print 'ERROR: give a number as argument'
        return None

    jd=float(x)
    jd=jd+0.5
    Z=int(jd)
    F=jd-Z
    alpha=int((Z-1867216.25)/36524.25)
    A=Z + 1 + alpha - int(alpha/4)

    B = A + 1524
    C = int( (B-122.1)/365.25)
    D = int( 365.25*C )
    E = int( (B-D)/30.6001 )

    dd = B - D - int(30.6001*E) + F

    if E<13.5:
	mm=E-1

    if E>13.5:
	mm=E-13

    if mm>2.5:
	yyyy=C-4716

    if mm<2.5:
	yyyy=C-4715

    months=["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
    daylist=[31,28,31,30,31,30,31,31,30,31,30,31]
    daylist2=[31,29,31,30,31,30,31,31,30,31,30,31]

    h=int((dd-int(dd))*24)
    mins=int((((dd-int(dd))*24)-h)*60)
    sec=86400*(dd-int(dd))-h*3600-mins*60
    isec=int(sec)
    usec=int((sec-isec)*1000000)


    gd=dt.datetime(yyyy,mm,int(dd),h,mins,isec,usec)

    return gd

def mjd2gd(x):
    return jd2gd(x+2400000.5)
