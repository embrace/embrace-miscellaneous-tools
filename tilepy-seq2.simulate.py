"""
Test program for Embrace tiles.
Envoie une sequence de startup-shutdown sur les tuiles d'un tileset pour tester le pointage.
"""
import os,time

totalSleepTime=0.0
summedPower=[]
timeaxis=[]
tilestate=[0,0,0,0]

def CmdStartup(numtileset,numtile):
    global totalSleepTime,tileset

    tileref='T'+str(numtileset)+'.'+str(numtile)
    #print 'tileref=',tileref
    cmd='python tilepy.py --tile='+tileref+' --nostate --fwversion'
    #rep=os.popen(cmd).read()
    #print 'rep=',rep
    totalSleepTime+=0.1
    cmd='python tilepy.py --tile='+tileref+' --nostate --fwversion --startup'
    #rep=os.popen(cmd).read()
    #print 'rep=',rep
    totalSleepTime+=1
    cmd='python tilepy.py --tile='+tileref+' --nostate --setregs=[0]'
    #rep=os.popen(cmd).read()
    #print 'rep=',rep
    cmd='python tilepy.py --tile='+tileref+' --nostate  --sync'
    #rep=os.popen(cmd).read()
    #print 'rep=',rep

    totalSleepTime+=0.1
    tilestate[numtile-1]=1

def CmdShutDown(numtileset,numtile):
    global totalSleepTime,tilestate

    tileref='T'+str(numtileset)+'.'+str(numtile)

    #print 'tileref=',tileref
    cmd='python tilepy.py --tile='+tileref+' --nostate --fwversion'
    #rep=os.popen(cmd).read()
    #print 'rep=',rep
    totalSleepTime+=0.1
    cmd='python tilepy.py --tile='+tileref+' --nostate --fwversion --shutdown'
    #rep=os.popen(cmd).read()
    #print 'rep=',rep

    totalSleepTime+=0.1
    tilestate[numtile-1]=0
    
    
#main script
print "ShutDown all tiles"
#for numtileset in [1,2,3,5,6,7,9,10,11,13,14,15]:
for numtileset in [1]:
    for numtile in [1,2,3,4]:
        CmdShutDown(numtileset,numtile)

print
print "Start ON/OFF sequence"
nloops=100
cycle=1
while(cycle<nloops):
    for numtile_on in [1,2,3,4]:
        #print
        #print "Tile ",numtile_on," is ON"
        #print "wait 10 seconds"
        totalSleepTime+=10
        #for numtileset in [1,2,3,5,6,7,9,10,11,13,14,15]:
        for numtileset in [1]:

            for numtile in [1,2,3,4]:
                tileref='T'+str(numtileset)+'.'+str(numtile)
                print totalSleepTime," ",sum(tilestate)
                summedPower.append(sum(tilestate))
                timeaxis.append(totalSleepTime)
                if(numtile == numtile_on):
                    CmdStartup(numtileset,numtile)
                else:
                    CmdShutDown(numtileset,numtile)

                #print "wait 10 seconds"
                print totalSleepTime," ",sum(tilestate)
                summedPower.append(sum(tilestate))
                timeaxis.append(totalSleepTime)
                totalSleepTime+=10 

            cycle+=1


import matplotlib.pyplot as plt
plt.plot(timeaxis,summedPower)
#plt.plot(timeaxis,summedPower,'ro')
plt.show()
