"""
$Id: teststuff.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Thu 20 Mar 2014 10:47:37 CET

testing stuff
"""

import datetime as dt
import ephem as eph
import math
from datefunctions import *
# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
ObsTitle='teststuff'
for beam in beams:
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="teststuff"
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

for beam in beams:
    obsfunc[beam].teststuff()


print 'TEST:  at script level'
try:
    print 'TEST: LCUconnect__var=',LCUconnect__var
except:
    print 'TEST: LCUconnect__var is undefined'
    
try:
    print 'TEST: LCUip__var=',LCUip__var
except:
    print 'TEST: LCUip__var is undefined'
