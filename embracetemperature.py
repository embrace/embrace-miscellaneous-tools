#!/usr/bin/python
"""
$Id: python script to get data from the temperature sensor
$auth: Steve Torchinsky (torchinsky@obs-nancay.fr)
$created: Wed May 25 13:57:22 CEST 2011

The sensor is AKCP sensorProbe2 v 2.0
   http://www.akcp.com/support/customer-login/
   userid: satorchi
   passwd: 7jT4iTWT8BCO

It's in the EMBRACE container attached to machine 192.168.1.100
The data can be fetched in a browser, or otherwise at 
    http://192.168.1.100/graph?index=0&goffset=0&time=
The resulting html contains the data (it's not just an image)
The data is in the form of a string of numbers, 
and a date corresponding to the last measurement time
and there is the polling frequency (eg. 1 per minute).

A bash script was written to extract the data in ~/bin/embracetemperatures.sh
here is the sed stuff to extract the data from the html

wget -O /tmp/embracetemperatures.html 'http://192.168.1.100/graph?goffset=8640&index=0&time='


lastdate=`cat /tmp/embracetemperatures.html|sed -ne 's/.*{theClock=new Date("\(.*\)");clock();}.*/\1/' -ep`

datarray=`cat /tmp/embracetemperatures.html|sed -ne 's/.*var data=new Array\(.*?\)/\1/' -ne 's/^(//' -e 's/);.*//' -ep`

and here it is in python (hopefully)

$date: Fri Jul 22 15:18:24 CEST 2011

up to today there was a bug: I was using date format dd/mm/yyyy but
the output for "var lastTime" is mm/dd/yyyy.  All data is buggered up
from June/July but maybe retrievable, except there's nothing on
calendar days greater than 12.  The data is in:
  embracetemperatures_20110713-0000.txt
have to convert to a calendar time, then interchange day and month

The polling frequency is not exactly 1 minute.  It's more like 61 seconds
Here are some examples of "var lastTime"

"07/22/2011 14:52:51"
"07/22/2011 14:54:53"
"07/22/2011 14:55:55" this one is anomolous: 62 seconds
"07/22/2011 14:56:56"
"07/22/2011 15:08:07" 
"07/22/2011 15:12:11"
"07/22/2011 15:14:13"
"07/22/2011 15:17:16"
"07/22/2011 15:20:19"
"07/22/2011 15:38:37"
"07/22/2011 15:44:43"
"07/22/2011 15:48:47"

polltime=61.0 works better, but it's still not exact.

$date: Sat Jul 30 12:49:44 CEST 2011
   option to send mail with current temperature

"""

# import urllib to connect to the temperature server
import urllib as url
import re # regular expressions
import time
import matplotlib.pyplot as plt
import sys
import os

# get the latest data from the server

###### this URL gives only the latest data
handle=url.urlopen('http://192.168.1.100/graph?goffset=8640&index=0&time=')

##### this is the "zoom-out" version.  ie. all available data
# but the data points are a selection of the full set
# so it's better to use the zoom of the latest data and repoll every 10 mins
#handle=url.urlopen('http://192.168.1.100/graph2?index=0&time=')

# and save to a string
html=handle.readlines()
html=html[0]

# close the handle
handle.close()

# the data
datspan=re.search('data=new Array\(.*?\)',html).span()
datstr=html[datspan[0]+15:datspan[1]-1]
dat=eval(datstr)

# the date of the last reading
#tspan=re.search('theClock=new Date\(".*?"\);clock\(\);}',html).span()
#timestr=html[tspan[0]+19:tspan[1]-12]
tspan=re.search('var lastTime=new Date\(".*?"\);',html).span()
timestr=html[tspan[0]+23:tspan[1]-3]

# convert timestring into minutes etc
time_final=time.strptime(timestr,"%m/%d/%Y %H:%M:%S")
t_final=time.mktime(time_final)

# the polling period in seconds
polltime=61.0

# make a list of pairs: (time, data)
data=[]
n=0
for d in reversed(dat):
    t=t_final-n*polltime
    #print t,d
    data.append( [t,d] )
    n+=1

# sort the data in chronological order, and remove duplicates
dataset = sorted(set( [tuple(xx) for xx in data] ))

# crop the list because the first two points are always zeros
n = len(dataset)
#print 'number of points: ',n
#print 'data 0: ',dataset[0]
#print 'data 1: ',dataset[1]

dataset=dataset[2:n]
n = len(dataset)
#print 'number of points: ',n

# save data to file
datfile = open(os.environ['HOME']+'/data/embracetemperatures.txt', 'a')
for d in dataset:
  s=str(d[0])+" "+str(d[1])+"\n"
  datfile.write(s)
datfile.close()

# send a mail with the current temperature
# following example at: http://docs.python.org/library/email-examples.html
import smtplib
from email.mime.text import MIMEText
for arg in sys.argv:
    if arg == '--mail':
        currtemp=dataset[len(dataset)-1][1]
        subj='EMBRACE Temperature: '+str(currtemp)+'C'
        if currtemp > 34:
            subj='ALERT! AIR CONDITIONER BREAKDOWN:  '+subj
        elif currtemp > 31:
            subj='WARNING! '+subj

        msg = MIMEText('This is the regularly scheduled EMBRACE Temperature notification.')
        msg['Subject']=subj
        msg['To']='Steve Torchinsky <torchinsky@obs-nancay.fr>'
        msg['From']='EMBRACE Container <torchinsky@obs-nancay.fr>'
        s = smtplib.SMTP('smtp.obs-nancay.fr')
        s.sendmail('EMBRACE Container <torchinsky@obs-nancay.fr>', 'Steve Torchinsky <torchinsky@obs-nancay.fr', msg.as_string())
        s.quit()
    
    # plot
    if arg == '--plot':
        dat=[tuple(xx)[1] for xx in dataset]
        fig=plt.figure()
        fig.canvas.set_window_title('EMBRACE Temperature') 
        plt.xlabel('minutes (up to '+timestr+')')
        plt.ylabel('Temperature / Celsius')
        plt.title('EMBRACE Temperature in the Container')
        ret=plt.plot(dat)
        ret=plt.show()













